package ru.testdev.instasearch.data.controllers;

import ru.testdev.instasearch.model.User;
import ru.testdev.instasearch.utils.Prefs;

public class LoginController {

	private static final String ACCESS_TOKEN = "access_token";
	private static final String PROFILE = "profile";

	private static final LoginController ourInstance = new LoginController();

	public static LoginController getInstance() {
		return ourInstance;
	}

	private LoginController() {
	}

	private String accessToken;
	private User user;

	public String getAccessToken() {
		if (accessToken != null) {
			return accessToken;
		} else {
			accessToken = Prefs.get().getString(ACCESS_TOKEN, null);
			return accessToken;
		}
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
		Prefs.edit().putString(ACCESS_TOKEN, accessToken).apply();
	}

	public User getUser() {
		if (user != null) {
			return user;
		} else {
			user = Prefs.load(PROFILE, User.class);
			return user;
		}
	}

	public void setUser(User user) {
		this.user = user;
		Prefs.save(PROFILE, user);
	}

	public void clear(){
		setAccessToken(null);
		setUser(null);
	}

	public boolean isLogin(){
		return getAccessToken() != null;
	}
}