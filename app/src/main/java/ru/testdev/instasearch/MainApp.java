package ru.testdev.instasearch;

import android.app.Application;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;

import ru.testdev.instasearch.network.NetworkStateReceiver;
import ru.testdev.instasearch.network.RestApi;
import ru.testdev.instasearch.ui.MainActivity;
import ru.testdev.instasearch.utils.Prefs;

/**
 * Created by medium on 01.07.17.
 */

public class MainApp extends Application {

	@Override
	public void onCreate() {
		super.onCreate();

		registerReceiver(new NetworkStateReceiver(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
		Prefs.init(this);

		RestApi.init(this, (route, response) -> {
			Prefs.clean();
			Intent intent = new Intent(this, MainActivity.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
			startActivity(intent);
			return null;
		});
	}
}
