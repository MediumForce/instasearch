package ru.testdev.instasearch.network;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import okhttp3.Authenticator;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

import static ru.testdev.instasearch.network.NetworkConst.BASE_URL;

/**
 * Created by medium on 01.07.17.
 */

public class RestApi {


	private static Retrofit retrofit = null;

	public static void init(Context context, Authenticator authenticator) {
		OkHttpClient.Builder okHttpClient = new OkHttpClient.Builder()
				.authenticator(authenticator);

		HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
		interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
		okHttpClient.addInterceptor(interceptor);

		Gson gson = new GsonBuilder().create();
		retrofit = new Retrofit.Builder()
				.addConverterFactory(GsonConverterFactory.create(gson))
				.addCallAdapterFactory(RxJavaCallAdapterFactory.create())
				.baseUrl(BASE_URL)
				.client(okHttpClient.build())
				.build();
	}

	public static <T> T createService(Class<T> serviceClass) {
		if (retrofit == null) {
			throw new IllegalStateException("Call `RestApi.init(Context, Authenticator)` before calling this method.");
		}
		return retrofit.create(serviceClass);
	}
}
