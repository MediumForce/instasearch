package ru.testdev.instasearch.network.serivces;


import retrofit2.Response;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import ru.testdev.instasearch.network.response.AccessTokenResponse;
import rx.Single;

/**
 * Created by medium on 28.06.17.
 */

public interface AuthService {
	@FormUrlEncoded
	@POST("oauth/access_token")
	Single<Response<AccessTokenResponse>> getAccessToken(@Field("client_id") String clientId,
	                                                     @Field("client_secret") String clientSecret,
	                                                     @Field("grant_type") String grantType,
	                                                     @Field("redirect_uri") String redirectUri,
	                                                     @Field("code") String code);
	
}
