package ru.testdev.instasearch.network;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.content.LocalBroadcastManager;


public class NetworkStateReceiver extends BroadcastReceiver {

	public static final String NETWORK_STATE = "ru.testdev.instasearch.network.NetworkStateReceiver.REQUEST_PROCESSED";

	public static final String EXTRA_NETWORK_STATE = "network_state";

	@Override
	public void onReceive(Context context, Intent intent) {
		int status = getConnectivityStatusString(context);
		if ("android.net.conn.CONNECTIVITY_CHANGE".equals(intent.getAction())) {
			Intent intentResult = new Intent(NETWORK_STATE);
			intentResult.putExtra(EXTRA_NETWORK_STATE, status != NETWORK_STATUS_NOT_CONNECTED);
			LocalBroadcastManager broadcaster = LocalBroadcastManager.getInstance(context);
			broadcaster.sendBroadcast(intentResult);

		}
	}

	public static int TYPE_WIFI = 1;
	public static int TYPE_MOBILE = 2;
	public static int TYPE_NOT_CONNECTED = 0;
	public static final int NETWORK_STATUS_NOT_CONNECTED = 0, NETWORK_STAUS_WIFI = 1, NETWORK_STATUS_MOBILE = 2;

	public static int getConnectivityStatus(Context context) {
		ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

		NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
		if (null != activeNetwork) {
			if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI)
				return TYPE_WIFI;

			if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE)
				return TYPE_MOBILE;
		}
		return TYPE_NOT_CONNECTED;
	}

	public static int getConnectivityStatusString(Context context) {
		int conn = getConnectivityStatus(context);
		int status = 0;
		if (conn == TYPE_WIFI) {
			status = NETWORK_STAUS_WIFI;
		} else if (conn == TYPE_MOBILE) {
			status = NETWORK_STATUS_MOBILE;
		} else if (conn == TYPE_NOT_CONNECTED) {
			status = NETWORK_STATUS_NOT_CONNECTED;
		}
		return status;
	}
}
