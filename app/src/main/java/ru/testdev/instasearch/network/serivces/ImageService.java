package ru.testdev.instasearch.network.serivces;

import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.http.Url;
import ru.testdev.instasearch.network.response.UserImagesResponse;
import rx.Single;

/**
 * Created by medium on 30.06.17.
 */

public interface ImageService {

	@GET
	Single<Response<UserImagesResponse>> getUserImages(@Url String url,
	                                                   @Query("access_token") String accessToken,
	                                                   @Query("count") int count, @Query("max_id") String maxId);
}
