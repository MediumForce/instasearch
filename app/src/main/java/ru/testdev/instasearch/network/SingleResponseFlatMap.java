package ru.testdev.instasearch.network;

import retrofit2.Response;
import rx.Single;
import rx.functions.Func1;

public class SingleResponseFlatMap<T> implements Func1<Response<T>, Single<T>> {

	@Override
	public Single<T> call(Response<T> response) {
		if (!response.isSuccessful()) {
			return Single.error(new RuntimeException(String.valueOf(response.code())));
		} else {
			return Single.just(response.body());
		}
	}
}
