package ru.testdev.instasearch.network;

/**
 * Created by medium on 01.07.17.
 */

public class NetworkConst {
	public static final String TAG = "NetworkConst";
	public static final String BASE_URL = "https://api.instagram.com/";
	public static final String CLIENT_ID = "51ec46033b7e4e2ea3b49371497cc096";
	public static final String CLIENT_SECRET = "51b5942612cd419f845ee45711d6e43e";
	public static final String GRANT_TYPE = "authorization_code";
	public static final String REDIRECT_URI = "http://localhost/";
	public static final String TEST_USER_URL = NetworkConst.BASE_URL + "v1/users/5653133167/media/recent/";
}
