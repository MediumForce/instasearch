package ru.testdev.instasearch.ui.login;

import ru.testdev.instasearch.data.controllers.LoginController;
import ru.testdev.instasearch.network.NetworkConst;
import ru.testdev.instasearch.network.RestApi;
import ru.testdev.instasearch.network.SingleResponseFlatMap;
import ru.testdev.instasearch.network.response.AccessTokenResponse;
import ru.testdev.instasearch.network.serivces.AuthService;
import rx.SingleSubscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by medium on 01.07.17.
 */

public class SignInPresenterImpl implements SignInPresenter {

	private View view;
	private AuthService authService;
	private LoginController loginController;

	public SignInPresenterImpl(View view) {
		this.view = view;
		this.authService = RestApi.createService(AuthService.class);
		this.loginController = LoginController.getInstance();
	}

	@Override
	public void getAccessToken(String code) {
		authService.getAccessToken(NetworkConst.CLIENT_ID,NetworkConst.CLIENT_SECRET, NetworkConst.GRANT_TYPE,
				NetworkConst.REDIRECT_URI,code)
				.subscribeOn(Schedulers.io())
				.flatMap(new SingleResponseFlatMap<>())
				.observeOn(AndroidSchedulers.mainThread())
				.subscribe(new SingleSubscriber<AccessTokenResponse>() {
					@Override
					public void onSuccess(AccessTokenResponse accessTokenResponse) {
						loginController.setAccessToken(accessTokenResponse.getAccessToken());
						loginController.setUser(accessTokenResponse.getUser());
						view.showImageListFragment();
					}

					@Override
					public void onError(Throwable error) {
						error.printStackTrace();
						view.reloadWebView();
					}
				});
	}
}
