package ru.testdev.instasearch.ui.images;


import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;

import java.util.List;

import ru.testdev.instasearch.base.adapter.BaseAdapter;
import ru.testdev.instasearch.base.adapter.Loader;
import ru.testdev.instasearch.model.media.Media;
import ru.testdev.instasearch.network.response.Pagination;

/**
 * Created by medium on 01.07.17.
 */

public class MediaAdapter extends BaseAdapter<Media> {

	private boolean orientationLand;
	public static final String TAG = "MediaAdapter";

	public MediaAdapter(@NonNull Context context, @NonNull Loader loader, @NonNull LayoutInflater inflater,
	                    boolean orientationLand) {
		super(context, loader);
		this.orientationLand = orientationLand;
		if (orientationLand) {
			delegatesManager.addDelegate(new LandscapeMediaDelegate(inflater, context));
			delegatesManager.addDelegate(new LandscapeProgressDelegate<>(context));
		} else {
			delegatesManager.addDelegate(new PortraitMediaDelegate(inflater, context));
		}
	}


	@Override
	public int getItemCount() {
		if (orientationLand) {
			if (items.size() % 2 == 0 && !items.isEmpty() && items.get(items.size() - 1) != null
					|| items.size() % 2 == 0) {
				return items.size() / 2;
			} else {
				return items.size() / 2 + 1;
			}
		} else {
			return items.size();
		}
	}
}
