package ru.testdev.instasearch.ui.images;

import android.content.Context;
import android.support.annotation.NonNull;

import java.util.List;

import ru.testdev.instasearch.base.adapter.ProgressDelegate;

/**
 * Created by medium on 08.07.17.
 */

public class LandscapeProgressDelegate <T>extends ProgressDelegate<T> {
	public LandscapeProgressDelegate(Context context) {
		super(context);
	}

	@Override
	protected boolean isForViewType(@NonNull List<T> items, int position) {
		return items.get(position * 2) == null;
	}
}
