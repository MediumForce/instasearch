package ru.testdev.instasearch.ui.images;

import java.util.List;

import ru.testdev.instasearch.base.BaseView;
import ru.testdev.instasearch.model.media.Media;

/**
 * Created by medium on 30.06.17.
 */

public interface ImageListPresenter {

	void getImageList();

	void loadMore();

	String getMaxId();

	void setMaxId(String maxId);

	interface View extends BaseView{
		void swapMediaItems(List<Media> items);
		void addLoadedItems(List<Media> items);
		void addLastPage(List<Media> items);
	}
}
