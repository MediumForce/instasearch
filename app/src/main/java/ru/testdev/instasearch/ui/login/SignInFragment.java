package ru.testdev.instasearch.ui.login;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import ru.testdev.instasearch.R;
import ru.testdev.instasearch.base.BaseFragment;
import ru.testdev.instasearch.network.NetworkConst;
import ru.testdev.instasearch.network.RestApi;

public class SignInFragment extends BaseFragment implements SignInPresenter.View {

	private OnFragmentInteractionListener listener;

	private static final String TAG = "SignInFragment";

	private ClientListener clientListener;

	private WebView webView;

	SignInPresenter presenter;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	                         Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_sign_in, container, false);
		webView = (WebView) view.findViewById(R.id.web_view_fragment_sign_in);
		clientListener = new ClientListener() {
			@Override
			public void onComplete(String code) {
				Log.d(TAG, String.format("Code: %s", code));
				if (presenter != null) {
					presenter.getAccessToken(code);
				}
			}

			@Override
			public void onError(String error) {

			}
		};
		webView.setWebViewClient(new OAuthWebViewClient());
		reloadWebView();
		return view;
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		webView = null;
	}

	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
		if (context instanceof OnFragmentInteractionListener) {
			listener = (OnFragmentInteractionListener) context;
		} else {
			throw new RuntimeException(context.toString()
					+ " must implement OnFragmentInteractionListener");
		}
		presenter = new SignInPresenterImpl(this);
	}

	@Override
	public void onDetach() {
		super.onDetach();
		listener = null;
		presenter = null;
	}

	@Override
	public void showImageListFragment() {
		if (listener != null) {
			listener.showImageListFragment();
		}
	}

	@Override
	public void reloadWebView() {
		String url = String.format("https://api.instagram.com/oauth/authorize/?client_id=%s" +
						"&redirect_uri=%s" +
						"&response_type=code&display=touch&scope=public_content",
				NetworkConst.CLIENT_ID,
				NetworkConst.REDIRECT_URI);
		Log.d(TAG, "WebView URL: " + url);
		if (webView != null) {
			webView.loadUrl(url);
		}
	}

	public interface OnFragmentInteractionListener {
		void showImageListFragment();
	}

	private class OAuthWebViewClient extends WebViewClient {
		private static final String TAG = "Instagram-WebView";

		@SuppressWarnings("deprecation")
		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			Log.d(TAG, "Redirecting URL " + url);

			if (url.startsWith(NetworkConst.BASE_URL)) {
				String urls[] = url.split("=");
				clientListener.onComplete(urls[1]);
				return true;
			}
			return false;
		}

		@TargetApi(Build.VERSION_CODES.N)
		@Override
		public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
			String url = request.getUrl().toString();
			Log.d(TAG, "Redirecting URL " + url);

			if (url.startsWith(NetworkConst.REDIRECT_URI)) {
				String urls[] = url.split("=");
				clientListener.onComplete(urls[1]);
				return true;
			}
			return false;
		}

		@SuppressWarnings("deprecation")
		@Override
		public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
			Log.d(TAG, "Page error: " + description);

			super.onReceivedError(view, errorCode, description, failingUrl);
			clientListener.onError(description);
		}

		@TargetApi(android.os.Build.VERSION_CODES.M)
		@Override
		public void onReceivedError(WebView view, WebResourceRequest req, WebResourceError rerr) {
			onReceivedError(view, rerr.getErrorCode(), rerr.getDescription().toString(), req.getUrl().toString());
		}

	}


	private interface ClientListener {
		void onComplete(String code);

		void onError(String error);
	}
}
