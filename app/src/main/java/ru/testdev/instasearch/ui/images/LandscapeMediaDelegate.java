package ru.testdev.instasearch.ui.images;

import android.content.Context;
import android.content.res.Configuration;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.hannesdorfmann.adapterdelegates3.AdapterDelegate;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import ru.testdev.instasearch.R;
import ru.testdev.instasearch.model.media.Media;
import ru.testdev.instasearch.model.media.Videos;

/**
 * Created by medium on 08.07.17.
 */

public class LandscapeMediaDelegate extends AdapterDelegate<List<Media>> {

	private LayoutInflater inflater;
	private Context context;

	public LandscapeMediaDelegate(LayoutInflater inflater, Context context) {
		this.inflater = inflater;
		this.context = context;
	}

	@Override
	protected boolean isForViewType(@NonNull List<Media> items, int position) {
		return items.get(position * 2) != null;
	}

	@NonNull
	@Override
	protected RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent) {
		return new MediaViewHolder(inflater.inflate(R.layout.item_image_list, parent, false));
	}

	@Override
	protected void onBindViewHolder(@NonNull List<Media> items, int position,
	                                @NonNull RecyclerView.ViewHolder holder, @NonNull List<Object> payloads) {
		MediaViewHolder viewHolder = (MediaViewHolder) holder;
		Media mediaLeft = items.get(position * 2);
		onBindLeftItem(viewHolder, mediaLeft);
		if (items.size() - position * 2 > 1 && items.get(position * 2 + 1) != null) {
			viewHolder.mediaRightItem.setVisibility(View.VISIBLE);
			Media mediaRight = items.get(position * 2 + 1);
			onBindRightItem(viewHolder, mediaRight);
		} else {
			viewHolder.mediaRightItem.setVisibility(View.GONE);
		}

	}

	private void onBindLeftItem(MediaViewHolder viewHolder, Media item){
		if (item.isImage()) {
			Glide.with(context).load(item.getImages().getStandardResolution().getUrl())
					.placeholder(R.drawable.media_place_holder).dontAnimate().diskCacheStrategy(DiskCacheStrategy.RESULT)
					.fitCenter().into(viewHolder.mediaLeftItem);
		} else if (item.getVideos() != null) {
			Videos videos = item.getVideos();
			Glide.with(context).load(videos.getImages().getStandardResolution().getUrl())
					.placeholder(R.drawable.media_place_holder).dontAnimate().diskCacheStrategy(DiskCacheStrategy.RESULT)
					.fitCenter().into(viewHolder.mediaLeftItem);
		}
	}

	private void onBindRightItem(MediaViewHolder viewHolder, Media item){
		if (item.isImage()) {
			Glide.with(context).load(item.getImages().getStandardResolution().getUrl())
					.placeholder(R.drawable.media_place_holder).dontAnimate().diskCacheStrategy(DiskCacheStrategy.RESULT)
					.fitCenter().into(viewHolder.mediaRightItem);
		} else if (item.getVideos() != null) {
			Videos videos = item.getVideos();
			Glide.with(context).load(videos.getImages().getStandardResolution().getUrl())
					.placeholder(R.drawable.media_place_holder).dontAnimate().diskCacheStrategy(DiskCacheStrategy.RESULT)
					.fitCenter().into(viewHolder.mediaRightItem);
		}
	}

	public static class MediaViewHolder extends RecyclerView.ViewHolder {
		ImageView mediaLeftItem;
		ImageView mediaRightItem;

		public MediaViewHolder(View itemView) {
			super(itemView);
			mediaLeftItem = (ImageView) itemView.findViewById(R.id.image_view_media_item_image_list);
			mediaRightItem = (ImageView) itemView.findViewById(R.id.image_view_media_right_item_image_list);
		}
	}
}
