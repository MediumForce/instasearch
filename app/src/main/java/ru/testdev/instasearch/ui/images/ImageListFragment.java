package ru.testdev.instasearch.ui.images;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import ru.testdev.instasearch.R;
import ru.testdev.instasearch.base.BaseFragment;
import ru.testdev.instasearch.base.adapter.BaseAdapter;
import ru.testdev.instasearch.model.media.Media;

public class ImageListFragment extends BaseFragment implements ImageListPresenter.View {

	public static final String TAG = "ImageListFragment";

	private static final String FIRST_VISIBLE_ITEM = "first_visible_item";
	private static final String LOADED_ITEMS = "loaded_items";
	private static final String NEXT_MAX_ID = "next_max_id";
	private static final String IS_LAST_PAGE = "is_last_page";
	private static final String PAST_ORIENTATION = "past_orientation";
	private static final int SCROLL_DELAY = 150;

	private OnFragmentInteractionListener listener;

	private ImageListPresenter presenter;

	private TextView hint;

	private SwipeRefreshLayout refreshLayout;

	private RecyclerView recyclerView;

	private MediaAdapter adapter;

	private LinearLayoutManager layoutManager;
	Handler handler;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	                         Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_image_list, container, false);
		hint = (TextView) view.findViewById(R.id.text_view_fragment_image_list_hint);
		recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view_image_list_fragment);
		refreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_fragment_image_list);
		layoutManager = new LinearLayoutManager(getContext());
		adapter = new MediaAdapter(getContext(), (offset, params) -> presenter.loadMore(), inflater, isLandOrientation());
		recyclerView.setLayoutManager(layoutManager);
		recyclerView.setAdapter(adapter);
		adapter.setLoadDetector(BaseAdapter.Direction.TO_END, recyclerView);
		refreshLayout.setOnRefreshListener(() -> presenter.getImageList());
		handler = new Handler();
		return view;
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		hint = null;
		layoutManager = null;
		refreshLayout = null;
		adapter.removeLoadDetector(recyclerView);
		recyclerView = null;
		adapter = null;
		handler = null;
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		adapter.getItems().removeIf(media -> media == null);
		if (layoutManager.findFirstCompletelyVisibleItemPosition() == -1) {
			outState.putInt(FIRST_VISIBLE_ITEM, layoutManager.findFirstVisibleItemPosition());
		} else {
			outState.putInt(FIRST_VISIBLE_ITEM, layoutManager.findFirstCompletelyVisibleItemPosition());
		}
		outState.putParcelableArrayList(LOADED_ITEMS, (ArrayList<? extends Parcelable>) adapter.getItems());
		outState.putString(NEXT_MAX_ID, presenter.getMaxId());
		outState.putBoolean(IS_LAST_PAGE, adapter.isLastPage());
		if (isLandOrientation()) {
			outState.putInt(PAST_ORIENTATION, OrientationHelper.HORIZONTAL);
		} else {
			outState.putInt(PAST_ORIENTATION, OrientationHelper.VERTICAL);
		}
	}

	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		if (savedInstanceState != null) {
			adapter.swapItems(savedInstanceState.getParcelableArrayList(LOADED_ITEMS));
			adapter.setLastPage(savedInstanceState.getBoolean(IS_LAST_PAGE));
			presenter.setMaxId(savedInstanceState.getString(NEXT_MAX_ID));
			int pastOrientation = savedInstanceState.getInt(PAST_ORIENTATION);
			int firstVisibleItem = savedInstanceState.getInt(FIRST_VISIBLE_ITEM);
			if (isLandOrientation() && pastOrientation == OrientationHelper.HORIZONTAL) {
				handler.postDelayed(() -> scrollToPositionWithOffset(firstVisibleItem / 2, 0), SCROLL_DELAY);
			} else if (!isLandOrientation() && pastOrientation == OrientationHelper.VERTICAL) {
				handler.postDelayed(() -> scrollToPositionWithOffset(firstVisibleItem * 2, 0), SCROLL_DELAY);
			} else {
				handler.postDelayed(() -> scrollToPositionWithOffset(firstVisibleItem, 0), SCROLL_DELAY);
			}
		}
	}

	private void scrollToPositionWithOffset(int position, int offset) {
		if (layoutManager != null) {
			layoutManager.scrollToPositionWithOffset(position, offset);
		}
	}

	@Override
	public void onStart() {
		super.onStart();
		if (adapter.getItems().isEmpty()) {
			presenter.getImageList();
		}
	}

	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
		if (context instanceof OnFragmentInteractionListener) {
			listener = (OnFragmentInteractionListener) context;
		} else {
			throw new RuntimeException(context.toString()
					+ " must implement OnFragmentInteractionListener");
		}
		presenter = new ImageListPresenterImpl(this);
	}

	@Override
	public void onDetach() {
		super.onDetach();
		listener = null;
	}

	@Override
	public void swapMediaItems(List<Media> items) {
		if (adapter != null) {
			if (!items.isEmpty()) {
				hint.setVisibility(View.INVISIBLE);
				adapter.swapItems(items);
				adapter.setLastPage(false);
			} else {
				hint.setVisibility(View.VISIBLE);
			}
		}
	}

	@Override
	public void dismissLoadingDialog() {
		refreshLayout.setRefreshing(false);
	}

	@Override
	public void addLoadedItems(List<Media> items) {
		if (adapter != null) {
			adapter.addLoadItems(items);
		}
	}

	@Override
	public void addLastPage(List<Media> items) {
		if (adapter != null) {
			adapter.addLoadItems(items);
			adapter.setLastPage(true);
		}
	}

	private boolean isLandOrientation() {
		return getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE;
	}

	public interface OnFragmentInteractionListener {

	}
}
