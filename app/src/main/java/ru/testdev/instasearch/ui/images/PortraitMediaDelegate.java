package ru.testdev.instasearch.ui.images;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.hannesdorfmann.adapterdelegates3.AdapterDelegate;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import ru.testdev.instasearch.R;
import ru.testdev.instasearch.model.media.Media;
import ru.testdev.instasearch.model.media.Videos;

/**
 * Created by medium on 02.07.17.
 */

public class PortraitMediaDelegate extends AdapterDelegate<List<Media>> {

	private LayoutInflater inflater;
	private Context context;

	public PortraitMediaDelegate(LayoutInflater inflater, Context context) {
		this.inflater = inflater;
		this.context = context;
	}

	@Override
	protected boolean isForViewType(@NonNull List<Media> items, int position) {
		return items.get(position) != null;	}

	@NonNull
	@Override
	protected RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent) {
		return new MediaViewHolder(inflater.inflate(R.layout.item_image_list, parent, false));
	}

	@Override
	protected void onBindViewHolder(@NonNull List<Media> items, int position,
	                                @NonNull RecyclerView.ViewHolder holder, @NonNull List<Object> payloads) {
		MediaViewHolder viewHolder = (MediaViewHolder) holder;
		Media media = items.get(position);
		if (media.getUser() != null) {
			viewHolder.username.setText(media.getUser().getUsername());
			Glide.with(context).load(media.getUser().getProfilePicture()).into(viewHolder.avatar);
		}
		if (media.isImage()) {
			Glide.with(context).load(media.getImages().getStandardResolution().getUrl())
					.placeholder(R.drawable.media_place_holder).dontAnimate().diskCacheStrategy(DiskCacheStrategy.RESULT)
					.fitCenter().into(viewHolder.media);
			viewHolder.likes.setText(String.valueOf(media.getLikes().getCount()));
			viewHolder.comments.setText(String.valueOf(media.getComments().getCount()));
			viewHolder.createdTime.setText(getRelativeTime(media.getCreatedTime()));
		} else if (media.getVideos() != null) {
			Videos videos = media.getVideos();
			Glide.with(context).load(videos.getImages().getStandardResolution().getUrl())
					.placeholder(R.drawable.media_place_holder).dontAnimate().diskCacheStrategy(DiskCacheStrategy.RESULT)
					.fitCenter().into(viewHolder.media);
			viewHolder.likes.setText(String.valueOf(videos.getLikes().getCount()));
			viewHolder.comments.setText(String.valueOf(videos.getComments().getCount()));
			viewHolder.createdTime.setText(getRelativeTime(videos.getCreatedTime()));
		}
	}

	private CharSequence getRelativeTime(String time) {
		return DateUtils.getRelativeTimeSpanString(Long.parseLong(time) * 1000, System.currentTimeMillis(), DateUtils.DAY_IN_MILLIS);
	}

	public static class MediaViewHolder extends RecyclerView.ViewHolder {
		TextView username;
		TextView likes;
		TextView comments;
		TextView createdTime;
		ImageView media;
		CircleImageView avatar;
		public MediaViewHolder(View itemView) {
			super(itemView);
			username = (TextView) itemView.findViewById(R.id.text_view_username_item_image_list);
			likes = (TextView) itemView.findViewById(R.id.text_view_likes_item_image_list);
			comments = (TextView) itemView.findViewById(R.id.text_view_comments_item_image_list);
			createdTime = (TextView) itemView.findViewById(R.id.text_view_created_time_item_image_list);
			media = (ImageView) itemView.findViewById(R.id.image_view_media_item_image_list);
			avatar = (CircleImageView) itemView.findViewById(R.id.image_view_avatar_item_image_list);
		}
	}
}
