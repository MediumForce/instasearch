package ru.testdev.instasearch.ui.login;

import ru.testdev.instasearch.base.BaseView;

/**
 * Created by medium on 01.07.17.
 */

public interface SignInPresenter {

	void getAccessToken(String code);

	interface View extends BaseView {
		void showImageListFragment();
		void reloadWebView();
	}
}
