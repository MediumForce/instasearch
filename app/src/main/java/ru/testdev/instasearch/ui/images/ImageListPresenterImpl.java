package ru.testdev.instasearch.ui.images;

import ru.testdev.instasearch.base.adapter.LoadDetector;
import ru.testdev.instasearch.data.controllers.LoginController;
import ru.testdev.instasearch.network.NetworkConst;
import ru.testdev.instasearch.network.RestApi;
import ru.testdev.instasearch.network.SingleResponseFlatMap;
import ru.testdev.instasearch.network.response.UserImagesResponse;
import ru.testdev.instasearch.network.serivces.ImageService;
import rx.SingleSubscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by medium on 30.06.17.
 */

public class ImageListPresenterImpl implements ImageListPresenter {

	private View view;

	private ImageService imageService;
	private LoginController loginController;
	private String maxId;
	private static final int LIST_SIZE = LoadDetector.DEFAULT_ITEM_THRESHOLD;

	public ImageListPresenterImpl(View view) {
		this.view = view;
		imageService = RestApi.createService(ImageService.class);
		loginController = LoginController.getInstance();
	}

	@Override
	public void getImageList() {
		String AccessToken = loginController.getAccessToken();
		imageService.getUserImages(NetworkConst.TEST_USER_URL,
				AccessToken, LIST_SIZE, null).subscribeOn(Schedulers.io())
				.flatMap(new SingleResponseFlatMap<>())
				.observeOn(AndroidSchedulers.mainThread())
				.doAfterTerminate(() -> view.dismissLoadingDialog())
				.subscribe(new SingleSubscriber<UserImagesResponse>() {
					@Override
					public void onSuccess(UserImagesResponse userImagesResponse) {
						if (userImagesResponse.getPagination().getNextMaxId() != null) {
							maxId = userImagesResponse.getPagination().getNextMaxId();
							view.swapMediaItems(userImagesResponse.getData());
						} else {
							view.addLastPage(userImagesResponse.getData());
						}
					}

					@Override
					public void onError(Throwable error) {
						error.printStackTrace();
					}
				});
	}

	@Override
	public void loadMore() {
		String AccessToken = loginController.getAccessToken();
		imageService.getUserImages(NetworkConst.TEST_USER_URL,
				AccessToken, LIST_SIZE, maxId).subscribeOn(Schedulers.io())
				.flatMap(new SingleResponseFlatMap<>())
				.observeOn(AndroidSchedulers.mainThread())
				.doAfterTerminate(() -> view.dismissLoadingDialog())
				.subscribe(new SingleSubscriber<UserImagesResponse>() {
					@Override
					public void onSuccess(UserImagesResponse userImagesResponse) {
						if (userImagesResponse.getPagination().getNextMaxId()!=null) {
							maxId = userImagesResponse.getPagination().getNextMaxId();
							view.addLoadedItems(userImagesResponse.getData());
						} else {
							view.addLastPage(userImagesResponse.getData());
						}
					}

					@Override
					public void onError(Throwable error) {
						error.printStackTrace();
					}
				});
	}

	@Override
	public String getMaxId() {
		return maxId;
	}

	@Override
	public void setMaxId(String maxId) {
		this.maxId = maxId;
	}

}
