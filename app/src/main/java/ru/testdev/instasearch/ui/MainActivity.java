package ru.testdev.instasearch.ui;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import java.io.File;

import ru.testdev.instasearch.R;
import ru.testdev.instasearch.data.controllers.LoginController;
import ru.testdev.instasearch.ui.login.SignInFragment;
import ru.testdev.instasearch.ui.images.ImageListFragment;
import ru.testdev.instasearch.utils.Prefs;

public class MainActivity extends AppCompatActivity implements
		SignInFragment.OnFragmentInteractionListener,
		ImageListFragment.OnFragmentInteractionListener {

	private FragmentManager fragmentManager;

	public static final String IMAGE_LIST_FRAGMENT_TAG = "IMAGE_LIST_FRAGMENT";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);
		fragmentManager = getSupportFragmentManager();
		if (LoginController.getInstance().isLogin()) {
			showImageListFragment();
		} else {
			showSignInFragment();
		}
	}

	private void showSignInFragment() {
		SignInFragment signInFragment = new SignInFragment();
		FragmentTransaction transaction = fragmentManager.beginTransaction();
		transaction.replace(R.id.fragment_registration_container, signInFragment);
		transaction.commit();
	}


	@Override
	public void showImageListFragment() {
		ImageListFragment imageListFragment = (ImageListFragment) fragmentManager.findFragmentByTag(IMAGE_LIST_FRAGMENT_TAG);
		if (imageListFragment == null) {
			imageListFragment = new ImageListFragment();
		}
		FragmentTransaction transaction = fragmentManager.beginTransaction();
		transaction.replace(R.id.fragment_registration_container, imageListFragment, IMAGE_LIST_FRAGMENT_TAG);
		transaction.commit();
	}
}
