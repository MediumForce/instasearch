package ru.testdev.instasearch.base;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.support.design.widget.BaseTransientBottomBar;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.Toast;

import ru.testdev.instasearch.R;
import ru.testdev.instasearch.network.NetworkStateReceiver;

import static ru.testdev.instasearch.network.NetworkStateReceiver.NETWORK_STATUS_NOT_CONNECTED;


public class BaseFragment extends Fragment implements BaseView {
	protected Dialog dialog;

	public boolean isConnected() {
		int status = NetworkStateReceiver.getConnectivityStatusString(getContext());
		if (status == NETWORK_STATUS_NOT_CONNECTED) {
			showSnackBar(getString(R.string.all_snackbar_message_no_connected));
			return false;
		}
		return true;
	}

	@Override
	public void showSnackBar(String message) {
		View rootView = getActivity().getWindow().getDecorView().findViewById(android.R.id.content);
		Snackbar.make(rootView, message,
				BaseTransientBottomBar.LENGTH_LONG).show();
	}

	@Override
	public void showToast(String message) {
		Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
	}

	@Override
	public void showToast(String message, int length) {
		Toast.makeText(getContext(), message, length).show();
	}

	@Override
	public void showLoadingDialog(String message) {
		dialog = ProgressDialog.show(getContext(), null, message, true, false);
	}

	@Override
	public void dismissLoadingDialog() {
		dialog.dismiss();
	}
}
