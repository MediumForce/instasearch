package ru.testdev.instasearch.base.adapter;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;

/**
 * Created by medium on 02.07.17.
 */

public class LoadFromEndDetector extends LoadDetector {

	LoadFromEndDetector(int itemThreshold) {
		super(itemThreshold);
	}

	private final String TAG = "LoadFromEndDetector";

	@Override
	public void enableProgressItem(boolean isEnable) {
		if (isEnable) {
			adapter.add(null);
		} else {
			if (adapter.getItems().get(adapter.getItems().size() - 1) == null) {
				adapter.remove(adapter.getItems().size() - 1);
			}
		}
	}

	@Override
	public RecyclerView.OnScrollListener getScrollListener() {
		return new RecyclerView.OnScrollListener() {
			@Override
			public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
				super.onScrolled(recyclerView, dx, dy);
				if (dy <= 0) {
					//scroll up, all visible items already loaded
					return;
				}

				final int loadedItemsCount = adapter.getItemCount();
				final int lastVisibleItem = findLastVisibleItemPosition(layoutManager);
				if (!isLoading() && loadedItemsCount <= (lastVisibleItem + getItemThreshold())
						&& adapter.loader != null && !adapter.isLastPage()) {
					adapter.loadItems(loadedItemsCount);
				}
			}
		};
	}

	private int findLastVisibleItemPosition(RecyclerView.LayoutManager layoutManager) {
		if (layoutManager instanceof LinearLayoutManager) {
			return ((LinearLayoutManager) layoutManager).findLastVisibleItemPosition();
		} else {
			int[] pos = ((StaggeredGridLayoutManager) layoutManager).findLastVisibleItemPositions(null);
			return pos[pos.length - 1];
		}
	}
}
