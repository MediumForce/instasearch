package ru.testdev.instasearch.base.adapter;

/**
 * Created by medium on 03.07.17.
 */

public interface Loader {
	void onLoadMore(int offset, Object... params);
}
