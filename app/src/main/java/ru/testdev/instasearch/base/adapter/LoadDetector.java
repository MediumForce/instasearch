package ru.testdev.instasearch.base.adapter;

import android.support.v7.widget.RecyclerView;

import java.util.List;

/**
 * Created by medium on 02.07.17.
 */

public abstract class LoadDetector {

	public static final int DEFAULT_ITEM_THRESHOLD = 4;
	private volatile boolean isLoading;
	private final int itemThreshold;
	protected RecyclerView.LayoutManager layoutManager;
	protected BaseAdapter adapter;
	private RecyclerView.OnScrollListener scrollListener;

	LoadDetector(int itemThreshold) {
		this.itemThreshold = itemThreshold;
	}

	public void onAttachedToRecyclerView(RecyclerView recyclerView, BaseAdapter adapter) {
		this.layoutManager = recyclerView.getLayoutManager();
		this.adapter = adapter;
		scrollListener = getScrollListener();
		recyclerView.addOnScrollListener(scrollListener);
	}

	void onDetachedFromRecyclerView(RecyclerView recyclerView) {
		recyclerView.removeOnScrollListener(scrollListener);
		this.scrollListener = null;
		this.layoutManager = null;
		this.adapter = null;
	}


	public abstract void enableProgressItem(boolean isEnable);

	public abstract RecyclerView.OnScrollListener getScrollListener();

	public final void setLoadingState(boolean isLoading) {
		this.isLoading = isLoading;
		enableProgressItem(isLoading);
	}

	public final boolean isLoading() {
		return isLoading;
	}

	int getItemThreshold() {
		return itemThreshold;
	}


	/**
	 * Check if items fit screen
	 *
	 * @param loadedItemsCount loaded items count
	 * @return <tt>true</tt>, if items don't fit screen and need downloading, <tt>false</tt> - otherwise
	 */
	boolean isItemsNotFitScreen(int loadedItemsCount) {
		final int childCount = layoutManager.getChildCount();
		return childCount == 0 || adapter.getItemCount() - loadedItemsCount <= childCount;
	}
}
