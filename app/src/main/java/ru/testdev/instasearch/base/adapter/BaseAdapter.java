package ru.testdev.instasearch.base.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.hannesdorfmann.adapterdelegates3.AdapterDelegate;
import com.hannesdorfmann.adapterdelegates3.AdapterDelegatesManager;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * Created by medium on 02.07.17.
 */

public abstract class BaseAdapter<T> extends RecyclerView.Adapter{

	public @interface Direction {
		String TO_END = "end";
	}

	protected AdapterDelegatesManager<List<T>> delegatesManager;
	protected ProgressDelegate<T> progressDelegate;
	protected List<T> items;
	protected Context context;
	protected LoadDetector loadDetector;
	protected Loader loader;
	protected boolean isLastPage;

	public BaseAdapter(@NonNull Context context, @NonNull Loader loader){
		this.context = context;
		this.loader = loader;
		this.items = new ArrayList<>();
		delegatesManager = new AdapterDelegatesManager<>();
	}

	public void addDelegate(AdapterDelegate<List<T>> adapterDelegate){
		delegatesManager.addDelegate(adapterDelegate);
	}

	public void swapItems(List<T> newItems){
		if (items != null){
			items.clear();
			items.addAll(newItems);
		} else {
			items = newItems;
		}
		notifyDataSetChanged();
	}


	public void add(final T item) {
		add(item, items.size());
	}


	public void add(final T item, int startPosition) {
		if (startPosition > items.size()) {
			startPosition = items.size();
		}
		int initialSize = items.size();
		items.add(startPosition, item);
		int finalStartPosition = startPosition;
		if (initialSize == 0) {
			notifyDataSetChanged();
		} else {
			notifyItemInserted(finalStartPosition);
		}
	}


	@Nullable
	public T remove(int position) {
		if (position < 0 || position >= items.size()) {
			return null;
		}
		final T removed = items.remove(position);
		if (items.isEmpty()) {
			notifyDataSetChanged();
		} else {
			notifyItemRemoved(position);
		}
		return removed;
	}

	public void setLoadDetector(@NonNull @Direction String direction, @NonNull RecyclerView recyclerView){
		setLoadDetector(direction, recyclerView, LoadDetector.DEFAULT_ITEM_THRESHOLD);
	}

	public void setLoadDetector(@NonNull @Direction String direction, @NonNull RecyclerView recyclerView,
	                            int itemThreshold){
		if (direction.equals(Direction.TO_END)) {
			this.loadDetector = new LoadFromEndDetector(itemThreshold);
		} else {
			throw new IllegalArgumentException("wrong direction value");
		}
		progressDelegate = new ProgressDelegate<>(recyclerView.getContext());
		delegatesManager.addDelegate(progressDelegate);
		this.loadDetector.onAttachedToRecyclerView(recyclerView, this);
	}

	public void removeLoadDetector(@NonNull RecyclerView recyclerView){
		loadDetector.onDetachedFromRecyclerView(recyclerView);
		delegatesManager.removeDelegate(progressDelegate);
		loadDetector = null;
	}

	public void loadItems(int loadedItemsCount) {
			loadDetector.setLoadingState(true);
			loader.onLoadMore(loadedItemsCount);
	}

	public void addLoadItems(List<T> newItems){
		loadDetector.setLoadingState(false);
		if (items == null){
			items = newItems;
			notifyDataSetChanged();
		} else {
			int from = items.size();
			items.addAll(newItems);
			notifyItemRangeInserted(from, items.size());
		}
	}

	public List<T> getItems() {
		return items;
	}

	public boolean isLastPage() {
		return isLastPage;
	}

	public void setLastPage(boolean lastPage) {
		isLastPage = lastPage;
		for (int i = 0; i < items.size(); i++) {
			if (items.get(i) == null){
				remove(i);
			}
		}
	}

	@Override public int getItemViewType(int position) {
		return delegatesManager.getItemViewType(items, position);
	}

	@Override public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		return delegatesManager.onCreateViewHolder(parent, viewType);
	}

	@Override public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
		delegatesManager.onBindViewHolder(items, position, holder);
	}
}
