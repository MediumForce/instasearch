package ru.testdev.instasearch.base.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.hannesdorfmann.adapterdelegates3.AdapterDelegate;

import java.util.List;

import ru.testdev.instasearch.R;

public class ProgressDelegate<T> extends AdapterDelegate<List<T>> {
	private final LayoutInflater inflater;

	static class Progress {
	}

	public ProgressDelegate(Context context) {
		inflater = LayoutInflater.from(context);
	}

	@Override
	protected boolean isForViewType(@NonNull List<T> items, int position) {
		return items.get(position) == null;
	}

	@NonNull
	@Override
	public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent) {
		return new ProgressViewHolder(inflater.inflate(R.layout.loading_footer, parent, false));
	}

	@Override
	protected void onBindViewHolder(@NonNull List<T> items, int position,
	                                @NonNull RecyclerView.ViewHolder holder, @NonNull List<Object> payloads) {


	}

	static class ProgressViewHolder extends RecyclerView.ViewHolder {
		ProgressBar progressBar;

		ProgressViewHolder(View itemView) {
			super(itemView);
			progressBar = (ProgressBar) itemView;
		}
	}
}
