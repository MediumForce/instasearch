package ru.testdev.instasearch.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

public class Prefs {
	private static SharedPreferences settings = null;
	private static SharedPreferences.Editor editor = null;

	private static Gson gson;

	public static void init(Context context) {
		settings = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext());
		editor = settings.edit();
		gson = new Gson();
	}

	public static SharedPreferences get() {
		return settings;
	}

	public static SharedPreferences.Editor edit() {
		return editor;
	}

	public static void clean() {
		editor.clear().apply();
	}

	public static <T> void save(String key, T model) {
		edit().putString(key, gson.toJson(model, new TypeToken<T>() {}.getType())).apply();
	}

	public static <T> T load(String key, Type type) {
		String json = get().getString(key, null);
		if (json == null) {
			return null;
		} else {
			return gson.fromJson(json, type);
		}
	}

	public static <T> T load(String key, Class<T> tClass) {
		String json = get().getString(key, null);
		if (json == null) {
			return null;
		} else {
			return gson.fromJson(json, tClass);
		}
	}

	public static void remove(String... keys) {
		for (String key : keys) {
			editor.remove(key);
		}
		editor.apply();
	}
}
