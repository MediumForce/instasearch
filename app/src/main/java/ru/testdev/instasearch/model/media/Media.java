
package ru.testdev.instasearch.model.media;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import ru.testdev.instasearch.model.User;
import ru.testdev.instasearch.model.media.Caption;
import ru.testdev.instasearch.model.media.Comments;
import ru.testdev.instasearch.model.media.Images;
import ru.testdev.instasearch.model.media.Likes;
import ru.testdev.instasearch.model.media.Location;
import ru.testdev.instasearch.model.media.Videos;

public class Media implements Parcelable {

    public static final String IMAGE_TYPE = "image";
    public static final String VIDEO_TYPE = "video";

    @Override
    public String toString() {
        return "Media{" +
                "link='" + link + '\'' +
                '}';
    }

    @SerializedName("comments")
    @Expose
    private Comments comments;
    @SerializedName("caption")
    @Expose
    private Caption caption;
    @SerializedName("likes")
    @Expose
    private Likes likes;
    @SerializedName("link")
    @Expose
    private String link;
    @SerializedName("user")
    @Expose
    private User user;
    @SerializedName("created_time")
    @Expose
    private String createdTime;
    @SerializedName("images")
    @Expose
    private Images images;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("users_in_photo")
    @Expose
    private List<Object> usersInPhoto = null;
    @SerializedName("filter")
    @Expose
    private String filter;
    @SerializedName("tags")
    @Expose
    private List<String> tags = null;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("location")
    @Expose
    private Location location;
    @SerializedName("videos")
    @Expose
    private Videos videos;

    public boolean isImage(){
        return type.equals(IMAGE_TYPE);
    }

    public boolean isVideo(){
        return type.equals(VIDEO_TYPE);
    }

    public Comments getComments() {
        return comments;
    }

    public void setComments(Comments comments) {
        this.comments = comments;
    }

    public Caption getCaption() {
        return caption;
    }

    public void setCaption(Caption caption) {
        this.caption = caption;
    }

    public Likes getLikes() {
        return likes;
    }

    public void setLikes(Likes likes) {
        this.likes = likes;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(String createdTime) {
        this.createdTime = createdTime;
    }

    public Images getImages() {
        return images;
    }

    public void setImages(Images images) {
        this.images = images;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<Object> getUsersInPhoto() {
        return usersInPhoto;
    }

    public void setUsersInPhoto(List<Object> usersInPhoto) {
        this.usersInPhoto = usersInPhoto;
    }

    public String getFilter() {
        return filter;
    }

    public void setFilter(String filter) {
        this.filter = filter;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public Videos getVideos() {
        return videos;
    }

    public void setVideos(Videos videos) {
        this.videos = videos;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.comments, flags);
        dest.writeParcelable(this.caption, flags);
        dest.writeParcelable(this.likes, flags);
        dest.writeString(this.link);
        dest.writeParcelable(this.user, flags);
        dest.writeString(this.createdTime);
        dest.writeParcelable(this.images, flags);
        dest.writeString(this.type);
        dest.writeList(this.usersInPhoto);
        dest.writeString(this.filter);
        dest.writeStringList(this.tags);
        dest.writeString(this.id);
        dest.writeParcelable(this.location, flags);
        dest.writeParcelable(this.videos, flags);
    }

    public Media() {
    }

    protected Media(Parcel in) {
        this.comments = in.readParcelable(Comments.class.getClassLoader());
        this.caption = in.readParcelable(Caption.class.getClassLoader());
        this.likes = in.readParcelable(Likes.class.getClassLoader());
        this.link = in.readString();
        this.user = in.readParcelable(User.class.getClassLoader());
        this.createdTime = in.readString();
        this.images = in.readParcelable(Images.class.getClassLoader());
        this.type = in.readString();
        this.usersInPhoto = new ArrayList<>();
        in.readList(this.usersInPhoto, Object.class.getClassLoader());
        this.filter = in.readString();
        this.tags = in.createStringArrayList();
        this.id = in.readString();
        this.location = in.readParcelable(Location.class.getClassLoader());
        this.videos = in.readParcelable(Videos.class.getClassLoader());
    }

    public static final Parcelable.Creator<Media> CREATOR = new Parcelable.Creator<Media>() {
        @Override
        public Media createFromParcel(Parcel source) {
            return new Media(source);
        }

        @Override
        public Media[] newArray(int size) {
            return new Media[size];
        }
    };
}
