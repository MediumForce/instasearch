
package ru.testdev.instasearch.model.media;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Images implements Parcelable {

    @SerializedName("low_resolution")
    @Expose
    private Resolution lowResolution;
    @SerializedName("thumbnail")
    @Expose
    private Resolution thumbnail;
    @SerializedName("standard_resolution")
    @Expose
    private Resolution standardResolution;

    public Resolution getLowResolution() {
        return lowResolution;
    }

    public void setLowResolution(Resolution lowResolution) {
        this.lowResolution = lowResolution;
    }

    public Resolution getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(Resolution thumbnail) {
        this.thumbnail = thumbnail;
    }

    public Resolution getStandardResolution() {
        return standardResolution;
    }

    public void setStandardResolution(Resolution standardResolution) {
        this.standardResolution = standardResolution;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.lowResolution, flags);
        dest.writeParcelable(this.thumbnail, flags);
        dest.writeParcelable(this.standardResolution, flags);
    }

    public Images() {
    }

    protected Images(Parcel in) {
        this.lowResolution = in.readParcelable(Resolution.class.getClassLoader());
        this.thumbnail = in.readParcelable(Resolution.class.getClassLoader());
        this.standardResolution = in.readParcelable(Resolution.class.getClassLoader());
    }

    public static final Parcelable.Creator<Images> CREATOR = new Parcelable.Creator<Images>() {
        @Override
        public Images createFromParcel(Parcel source) {
            return new Images(source);
        }

        @Override
        public Images[] newArray(int size) {
            return new Images[size];
        }
    };
}
