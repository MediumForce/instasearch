
package ru.testdev.instasearch.model.media;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import ru.testdev.instasearch.model.User;

public class Videos implements Parcelable {

    @SerializedName("low_resolution")
    @Expose
    private Resolution lowResolution;
    @SerializedName("standard_resolution")
    @Expose
    private Resolution standardResolution;
    @SerializedName("comments")
    @Expose
    private Comments comments;
    @SerializedName("caption")
    @Expose
    private Caption caption;
    @SerializedName("likes")
    @Expose
    private Likes likes;
    @SerializedName("link")
    @Expose
    private String link;
    @SerializedName("created_time")
    @Expose
    private String createdTime;
    @SerializedName("images")
    @Expose
    private Images images;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("users_in_photo")
    @Expose
    private List<Object> usersInPhoto;
    @SerializedName("filter")
    @Expose
    private String filter;
    @SerializedName("tags")
    @Expose
    private List<Object> tags = null;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("user")
    @Expose
    private User user;
    @SerializedName("location")
    @Expose
    private Location location;

    public Resolution getLowResolution() {
        return lowResolution;
    }

    public void setLowResolution(Resolution lowResolution) {
        this.lowResolution = lowResolution;
    }

    public Resolution getStandardResolution() {
        return standardResolution;
    }

    public void setStandardResolution(Resolution standardResolution) {
        this.standardResolution = standardResolution;
    }

    public Comments getComments() {
        return comments;
    }

    public void setComments(Comments comments) {
        this.comments = comments;
    }

    public Caption getCaption() {
        return caption;
    }

    public void setCaption(Caption caption) {
        this.caption = caption;
    }

    public Likes getLikes() {
        return likes;
    }

    public void setLikes(Likes likes) {
        this.likes = likes;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(String createdTime) {
        this.createdTime = createdTime;
    }

    public Images getImages() {
        return images;
    }

    public void setImages(Images images) {
        this.images = images;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<Object>  getUsersInPhoto() {
        return usersInPhoto;
    }

    public void setUsersInPhoto(List<Object>  usersInPhoto) {
        this.usersInPhoto = usersInPhoto;
    }

    public String getFilter() {
        return filter;
    }

    public void setFilter(String filter) {
        this.filter = filter;
    }

    public List<Object> getTags() {
        return tags;
    }

    public void setTags(List<Object> tags) {
        this.tags = tags;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.lowResolution, flags);
        dest.writeParcelable(this.standardResolution, flags);
        dest.writeParcelable(this.comments, flags);
        dest.writeParcelable(this.caption, flags);
        dest.writeParcelable(this.likes, flags);
        dest.writeString(this.link);
        dest.writeString(this.createdTime);
        dest.writeParcelable(this.images, flags);
        dest.writeString(this.type);
        dest.writeList(this.usersInPhoto);
        dest.writeString(this.filter);
        dest.writeList(this.tags);
        dest.writeString(this.id);
        dest.writeParcelable(this.user, flags);
        dest.writeParcelable(this.location, flags);
    }

    public Videos() {
    }

    protected Videos(Parcel in) {
        this.lowResolution = in.readParcelable(Resolution.class.getClassLoader());
        this.standardResolution = in.readParcelable(Resolution.class.getClassLoader());
        this.comments = in.readParcelable(Comments.class.getClassLoader());
        this.caption = in.readParcelable(Object.class.getClassLoader());
        this.likes = in.readParcelable(Likes.class.getClassLoader());
        this.link = in.readString();
        this.createdTime = in.readString();
        this.images = in.readParcelable(Images.class.getClassLoader());
        this.type = in.readString();
        this.usersInPhoto = in.readParcelable(Object.class.getClassLoader());
        this.filter = in.readString();
        this.tags = new ArrayList<Object>();
        in.readList(this.tags, Object.class.getClassLoader());
        this.id = in.readString();
        this.user = in.readParcelable(User.class.getClassLoader());
        this.location = in.readParcelable(Object.class.getClassLoader());
    }

    public static final Parcelable.Creator<Videos> CREATOR = new Parcelable.Creator<Videos>() {
        @Override
        public Videos createFromParcel(Parcel source) {
            return new Videos(source);
        }

        @Override
        public Videos[] newArray(int size) {
            return new Videos[size];
        }
    };
}
