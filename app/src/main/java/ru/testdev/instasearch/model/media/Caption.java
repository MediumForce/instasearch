
package ru.testdev.instasearch.model.media;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import ru.testdev.instasearch.model.User;

public class Caption implements Parcelable {

    @SerializedName("created_time")
    @Expose
    private String createdTime;
    @SerializedName("text")
    @Expose
    private String text;
    @SerializedName("from")
    @Expose
    private User from;
    @SerializedName("id")
    @Expose
    private String id;

    public String getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(String createdTime) {
        this.createdTime = createdTime;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public User getFrom() {
        return from;
    }

    public void setFrom(User from) {
        this.from = from;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.createdTime);
        dest.writeString(this.text);
        dest.writeParcelable(this.from, flags);
        dest.writeString(this.id);
    }

    public Caption() {
    }

    protected Caption(Parcel in) {
        this.createdTime = in.readString();
        this.text = in.readString();
        this.from = in.readParcelable(User.class.getClassLoader());
        this.id = in.readString();
    }

    public static final Parcelable.Creator<Caption> CREATOR = new Parcelable.Creator<Caption>() {
        @Override
        public Caption createFromParcel(Parcel source) {
            return new Caption(source);
        }

        @Override
        public Caption[] newArray(int size) {
            return new Caption[size];
        }
    };
}
